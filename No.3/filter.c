#include <stdlib.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <dirent.h>

pid_t child_pid; //deklarasi var bertipe data pid_t yang bernama child_pid digunakan menyimpan pid
int status; //untuk menyimpan status

/*   Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola.
     Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”.
     Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.*/
void fileManagemen(){
    //deklarasi variabel tipe data char array 
    char file_url[] = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char file_name[] = "file.zip"; //file yang akan diunduh

    //proses untuk mendownload
    //melakukan fork (membuat proses child) dan menyimpan nilai pid
    child_pid = fork();
    if(child_pid == 0) {
        // jalankan program wget dengan argumen yang telah ditentukan (nama file yg diunduh dengan argumen)
        execlp("wget", "wget", "-O", file_name, file_url, NULL); //-O  digunakan untuk menentukan nama file yang akan disimpan dan URL, wget adalah "WEB GET" digunakan untuk mengunduh file dari internet
    }

    // proses untuk mengunzip
    //tunggu proses child selesai dengan wait
    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0) {
        //jalankan program unzip dengan argumen
        execlp("unzip", "unzip", file_name, NULL);
    }

    //Hapus file yang telah diunduh dan dieksteak sebelumnya
    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0) {
        execlp("rm", "rm", file_name, NULL); //rm adalah "REMOVE" untuk menghapus file
    }
}

//membuat beberapa directory yang akan digunakan
void buatGroup() {

    //buat directory penyerang
    child_pid = fork();
    //mengecek apakah proses child atau bukan
    if (child_pid == 0) {
        execlp("mkdir", "mkdir", "./players/penyerang", NULL);
    }

    //buat directory bek
    child_pid = fork();
    if (child_pid == 0) {
        execlp("mkdir", "mkdir", "./players/bek", NULL);
    }

    //buat directory gelandang
    child_pid = fork();
    if (child_pid == 0) {
        execlp("mkdir", "mkdir", "./players/gelandang", NULL);
    }

    // buat directory kiper
    child_pid = fork();
    if (child_pid == 0) {
        execlp("mkdir", "mkdir", "./players/kiper", NULL);
    }
}

/*   Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai
     dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. 
     Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.*/
void posisiGroup(char path[]) {
    buatGroup();
    //tunggu child selesai dieksekusi baru ke selanjutnya
    while ((wait(&status)) > 0);

    child_pid = fork();
    if (child_pid == 0) {
        //fungsi untuk mencari dan memindahkan file ke direktori tujuan
        execlp("find", "find", path, "-iname", "*Penyerang*", "-exec", "mv", "-n", "{}", "./players/penyerang/", ";", NULL);
       //path adalah direktori yang akan dicari file2nya. -iname menentukan bahwa pencarian dilakukan secara case sensitive. 
    }

    //kategori Bek
    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Bek*", "-exec", "mv", "-n", "{}", "./players/bek/", ";", NULL);
    }

    //kategori gelandang
    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Gelandang*", "-exec", "mv", "-n", "{}", "./players/gelandang/", ";", NULL);
    }

    //kategori kiper
    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Kiper*", "-exec", "mv", "-n", "{}", "./players/kiper/", ";", NULL);
    }
}

/*   Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU
     berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain).
     Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/*/
void buatTim(int bek, int gelandang, int penyerang){

    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/kiper | sort -t _ -nk4 | tail -n 1 >> ~/Formation_%d_%d_%d.txt", bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/bek | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",bek, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/gelandang | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",gelandang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/penyerang | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",penyerang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }
}

/* Dikarenakan database yang diunduh masih data mentah.
   Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.*/
void hapusbukanMU(char path[], char filename[]) { 

    child_pid = fork();
    if (child_pid == 0) {
        //find digunakan untuk mencari. path sebagai lokasi pencarian. -type untuk mencari file. ! -name filename digunakan untuk mengabaikan file dengan nama tertentu. -delete untuk menghapus file yang memenuhi kriteria
        execlp("find", "find", path, "-type", "f", "!", "-name", filename, "-delete", NULL);
    }
}
//Eksekusi Perintah
int main() {
    int status;

    fileManagemen();

    hapusbukanMU("./players", "*ManUtd*");

    posisiGroup("./players");

    buatTim(4, 3, 3);

    return 0;
}

