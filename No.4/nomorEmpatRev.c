#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

int main(int argc, char *argv[])
{
  if (argc != 5)
  {
    // exp ./nomorEmpat 0 0 15 /home/Documents/sisop/P2S2/hellow.sh
    puts("Tolong input dalam format [jam/'*'] [menit/'*'] [detik/'*'] [filePath]");
    return 1;
  }

  char *strHour = argv[1];
  char *strMin = argv[2];
  char *strSec = argv[3];
  char *filePath = argv[4];

  int hour, mins, secs;

  bool error = false;
  if (strHour[0] == '*')
    hour = -1;
  else
  {
    // convert string hour menjadi integer
    hour = atoi(strHour);

    if (hour > 23 || hour < 0){
      puts("Gaada jam segitu masbro");
      error = true;
    }
  }

  if (strMin[0] == '*')
    mins = -1;
  else
  {
    mins = atoi(strMin);

    if (mins > 59 || mins < 0){
      puts("Gaada menit segitu masbro");
      error = true;
    }
  }

  if (strSec[0] == '*')
    secs = -1;
  else
  {
    secs = atoi(strSec);

    if (secs > 59 || secs < 0){
      puts("Gaada detik segitu masbro");
      error = true;
    }
  }

  if (error){
    return 1;
  }

  pid_t pid;
  pid = fork();

  if (pid < 0) exit(0);
  else if (pid > 0) exit(1);
  else {
    
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    umask(0);

    pid_t executesid = setsid();
    if (executesid < 0) exit(0);

    while (1)
    {
      time_t now = time(NULL);
      struct tm *tm_now = localtime(&now);

      if ((hour == -1 || tm_now->tm_hour == hour) && (mins == -1 || tm_now->tm_min == mins) && (secs == -1 || tm_now->tm_sec == secs))
      {
        pid_t pid2 = fork();

        if (!pid2)
        {
          execl("/usr/bin/bash", "bash", filePath, NULL);
          exit(0);
        }
      }
      sleep(1);
    }
  }
}