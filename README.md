#  Sistem Operasi Modul 2
### Kelompok D09
| Nama | NRP |
| ------ | ------ |
| Christian Kevin Emor | 5025211153 |
| Andrian Tambunan | 5025211018 |
| Helmi Abiyu Mahendra | 5025211061 |


### Problem 1
##### Penjelasan Soal
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang berikut : 
https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
1. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
2. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
3. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
4. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

##### Solusi
Pada program ini diperlukan beberapa fungsi, yaitu `downloadFile()`, `unzipFile()`, `getFile()`, `createDirectory()`, `zipAnimal()`, `deleteDirectory()`

###### `downloadFile()`

```
void downloadFile(char *filename, char *url){
    pid_t child = fork();
    if(child == 0){ //child yang akan dijalankan
        char *argv[] = {"wget", "-O", filename, url, "&", NULL};
        execv("/usr/bin/wget", argv);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
}
```
###### Penjelasan
Function `downloadFile()` yang memiliki dua parameter yaitu url dan namefile, yang berfungsi untuk melakukan download file dari internet menggunakan program wget pada sistem operasi Linux.
1. Pemanggilan sistem `fork()` yang menghasilkan dua proses yang berbeda, yaitu parent process dan child process.
2. `char *argv[]` digunakan untuk mendeklarasikan sebuah array string untuk menyimpan argumen baris perintah yang akan diteruskan ke fungsi `execv()`.
3. `wget` merupakan sebuah program yang digunakan untuk mengunduh file dari internet.
4. `-0` merupakan opsi untuk menentukan nama file output dari unduhan yang dilakukan.
5. `filename` adalah penginputan nama yang akan menjadi nama file.
6. `url` adalah link download gambar.
7. `execv` merupakan fungsi untuk menggantikan image program saat ini dengan image program baru yang disebut dalam argumen pertama tadi.
8. `&` merupakan fungsi untuk menjalankan command wget dalam background, sehingga child process dapat berlanjut tanpa menunggu operasi download selesai.
9. `NULL` sebagai penanda akhir dari array args

###### Proses
1. Function downloadFile() diawali dengan deklarasi variabel stat yang bertipe data integer dan IDdownload yang bertipe data pid_t (process ID).
2. Kemudian, dilakukan pemanggilan sistem fork() yang menghasilkan dua proses yang berbeda, yaitu parent process dan child process.
3. Proses child process yang dihasilkan oleh fork() kemudian akan menjalankan program wget untuk mendownload file dari url yang ditentukan dan menyimpan file tersebut dengan nama file yang ditentukan juga.

###### `unzipFile()`
```
void unzipFile(char *directory){
    pid_t child = fork();
    if(child == 0){ 
        char *argv[] = {"unzip", directory, NULL};
        execv("/usr/bin/unzip", argv);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
}
```
###### Penjelasan
Function `unzipFile()` memiliki satu parameter yaitu sourceDir, yang berfungsi untuk melakukan ekstraksi (unzip) file pada direktori tertentu menggunakan program unzip pada sistem operasi Linux.
1. `unzip` merupakan nama program unzip.
2. `execv` merupakan fungsi untuk menggantikan image program saat ini dengan image program baru yang disebut dalam argumen pertama tadi.

###### Proses
1. Function unzipFile() diawali dengan deklarasi variabel IDunzip yang bertipe data pid_t (process ID).
2. Kemudian, dilakukan pemanggilan sistem fork() yang menghasilkan dua proses yang berbeda, yaitu parent process dan child process.
3. Proses child process yang dihasilkan oleh fork() kemudian akan menjalankan program unzip untuk mengekstrak file pada direktori tertentu.

###### `getFile()`
```
void getFile(){
    pid_t child = fork();
    if(child == 0){
        struct dirent *entry;
        DIR *directory = opendir(".");
        if(directory == NULL){
            printf("Error open dir");
            exit(EXIT_FAILURE);
        }
        int count = 0;
        while ((entry = readdir(directory)) != NULL) {
            if (entry->d_type == DT_REG) {
                count++;
            }
        }
         char **files = malloc(count * sizeof(char *));
        if (files == NULL) {
            printf("Error malloc");
            exit(EXIT_FAILURE);
        }
        rewinddir(directory);
         int i = 0;
        while ((entry = readdir(directory)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".jpg")) {
                files[i] = entry->d_name;
                i++;
            }
        }
        closedir(directory);
        srand(time(NULL));
        int pickAcak = rand() % i;
        char penjagaan[50];
        strcpy(penjagaan, files[pickAcak]);
        printf("Hewan yang harus dijaga adalah %s\n", penjagaan);
        free(files);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    }
}
```

###### Penjelasan
Function `getFile()` memiliki satu parameter yaitu directory, yang berfungsi untuk mendapatkan file gambar (.jpg) dari direktori tertentu secara acak dan menuliskan nama file tersebut ke dalam file teks baru bernama "penjaga.txt".
1. `DIR *directory = opendir(".")` berfungsi untuk membuka direktori tempat program berjalan dan mengembalikan sebuah pointer ke struktur dir.
2. `int count = 0;` berfungsi untuk hitung berapa file yang ada.
3. `readdir` berfungsi untuk membaca setiap file dalam direktori tersebut
4. `malloc` berfungsi untuk mengalokasikan memori.
5. `rewinddir(directory)` berfungsi untuk mengembalikan pointer ke file pertama.
6. `closedir(directory)` berfungsi untuk menutup direktorinya.
7. `srand(time(NULL))` berfungsi untuk menentukan nilai seed untuk generator bilangan acak.
8. `free(files)` berfungsi untuk mengembalikan blok memori.

###### Proses
1. Function `getFile()` dimulai dengan mendeklarasikan variabel stat yang bertipe data int dan variabel path yang merupakan array of pointer bertipe data char sebanyak 100 elemen.
2. Dilakukan deklarasi struct dirent dan DIR untuk membaca isi dari direktori yang diinputkan.
3. Dilakukan pengacakan nama file dengan menggunakan fungsi rand() yang diinisialisasi dengan time(NULL) untuk men-generate nilai random yang berbeda setiap kali program dijalankan. Kemudian, dilakukan perulangan untuk membaca setiap file pada direktori tersebut dan menyimpan nama file dengan ekstensi ".jpg" ke dalam array path.
4. Kemudian, program memilih nama file secara acak dari array path dan menuliskan nama file tersebut ke dalam file teks baru bernama "penjaga.txt" dengan menggunakan fungsi fprintf() dan menampilkannya di terminal dengan kalimat `Hewan yang harus dijaga adalah (nama hewan random)`, maka program akan menutup file dengan fungsi fclose() dan menutup direktori dengan fungsi closedir().

###### `createDirectory()`
```
void createDirectory(){
    system("mkdir HewanDarat HewanAir HewanAmphibi");
    system("mv *amphibi.jpg HewanAmphibi");
    system("mv *darat.jpg HewanDarat");
    system("mv *air.jpg HewanAir");
}
```
###### Penjelasan
Function `newDirectory()` berfungsi untuk membuat tiga direktori baru dengan nama "HewanDarat", "HewanAir", dan "HewanAmphibi" menggunakan perintah mkdir dan memindahkan file foto kedalam masing-masing fie.
1. `    system("mkdir HewanDarat HewanAir HewanAmphibi")
` berfungsi untuk emmbuat 3 direktori, yaitu `HewanDarat`, `HewanAir`, `HewanAmphibi`.
2. `system("mv *amphibi.jpg HewanAmphibi")`, `system("mv *darat.jpg HewanDarat")`, `system("mv *air.jpg HewanAir");` berfungsi untuk memindahkan file berdasarkan nama foto.

###### Proses
1. Proses pembuatan direktori dilakukan menggunakan fungsi `system("mkdir (nama direktori)")` 
2. Lalu mengalokasikan file foto ke dalam direktori berdasarkan nama file.

###### `zipAnimal()`
```
void zipAnimal(){
    pid_t child = fork();
    if(child == 0){ 
        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/usr/bin/zip", argv);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
    child = fork();
    if(child == 0){ 
        char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/usr/bin/zip", argv);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
    child = fork();
    if(child == 0){ 
        char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/usr/bin/zip", argv);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
}
```
###### Penjelasan
Fungsi `zipAnimal()`  berfungsi untuk melakukan kompresi pada tiga folder berbeda yang bernama `HewanDarat`, `HewanAir`, dan `HewanAmphibi`. Fungsi ini menggunakan sistem operasi Unix/Linux untuk menjalankan perintah zip.
1. `execv()` mengeksekusi program zip dan melakukan kompresi pada direktori yang sesuai dengan argumen yang diberikan.

###### Proses
1. Pertama-tama membuat proses child menggunakan perintah `fork()`.
2. Jika proses tersebut adalah proses child yang dikembalikan oleh `fork()` adalah 0), maka fungsi `execv()` akan dijalankan.
3, Fungsi ini akan mengeksekusi program zip di direktori `/usr/bin/zip` dengan argumen yang didefinisikan dalam variabel argv.
4. Proses ini dilakukan untuk semua folder yang ingin dikompresi.

###### `deleteDirectory()`
```
void deleteDirectory(){
    system("rm -rf HewanAir HewanDarat HewanAmphibi binatang.zip");
}
```
###### Penjelasan
Fungsi `deleteDirectory() ` berfungsi untuk menghapus direktori awal seperti, `HewanAir`, `Hewandarat, `HewanAmphibi`, dan file `bintang.zip`.
1. `system()` digunakan untuk menjalankan perintah shell pada sistem operasi.
2. `rm` digunakan untuk menghapus sebuah file/ direktori.
3. parameter `-r` digunakan untuk menghapus direktori secara rekursif
4. parameter `-f` digunakan untuk memaksa penghapusan tanpa konfirmasi

###### Proses
1. menghapus direktori bernama `HewanAir`, `Hewandarat, `HewanAmphibi`, dan file `bintang.zip`.

###### `main()`
```
int main(){
    pid_t child_id;
    int status;
    downloadFile("binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq");
    while((wait(&status)) > 0);
    unzipFile("binatang.zip");
    while((wait(&status)) > 0);
    getFile();
    while((wait(&status)) > 0);
    createDirectory();
    while((wait(&status)) > 0);
    zipAnimal();
    while((wait(&status)) > 0);
    deleteDirectory();

```
###### Penjelasan
1. `while((wait(&status)) > 0)` digunakan untuk menunggu semua child process selesai dieksekusi sebelum parent process melanjutkan eksekusi selanjutnya.

###### Proses
1. Menjalankan semua fungsi yang berada di atas
#### Problem 2
##### Penjelasan Soal
Membantu Sucipto untuk menemukan inspirasi lukisan baru dengan cara mencarikan gambar-gambar di internet sebagai referensi :

##### Point a
Buat  folder dengan program C yang mendownload sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
###### Solusi
Dilakukan pembuatan folder yang dinamai dengan timestamp menggunakan strftime, yang mana programnya berjalan setiap 30 detik (sleep (30)). Masuk ke dalam folder untuk prosedur berkutnya
###### Code
```sh
while (1) {
        exeOne();
        // jeda pembuatan folder
        sleep(30);
    }
===============
char foldername[20];
time_t t = time(NULL);
struct tm *tm = localtime(&t);

// penamaan folder berdasarkan timestamp
strftime(foldername, sizeof(foldername), "%Y-%m-%d_%H:%M:%S", tm);
// buat folder dengan nama yang telah ditentukan barusan
mkdir(foldername, 0777);
// masuk ke dalam folder dengan change directory
chdir(foldername);
```

###### Penjelasan
strftime() is a function in C which is used to format date and time.

##### Point b
Tiap folder diisi 15 gambar dari https://picsum.photos/ yang didownload tiap 5 detik. gambar berukuran (t%1000)+50 pixel. dengan t merupakan Epoch Unix. Gambar juga dinamai dengan timestamp
###### Solusi
Setelah masuk ke dalam folder, kemudian dilakukan proses download gambar dari url dengan interval 5 detik (sleep(5)), penamaan dengan strftime seperti pada folder, serta penyimpanan file dengan execl
###### Code
```sh
char url[50];
    char *argv[5];
    time_t     now;
    struct tm  ts;
    char       buf[80];

    // Get current time
    time(&now);

    ts = *localtime(&now);
    // deklarasi epoch time
    time_t epoch;
    // penamaan berdasarkan timestamp
    if(strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts)) {
        epoch = mktime(&ts);
    }
    char filename[20];
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S.jpg", tm);
    sprintf(url, URL, epoch%1000 + 50);
    
    pid_t pid;
    
    pid = fork();
            
    if (pid == -1) {
        perror("fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", "-q", "-O", filename, url, NULL);
        exit(EXIT_SUCCESS);
    }
```
###### Penjelasan
*mktime() function - Convert broken-down time into time since the Epoch
*execl() function replaces the current process image with a new process image specified by path.

##### Point c
Folder yang sudah berisi 15 gambar kemudian di-zip, dan di delete (hanya menyisakan zip)
###### Solusi
Keluar dari direktori, zipping folder menggunakan execl, dan kemudian hapus dengan cara yg sama
###### code
```sh
// keluar dari direktori untuk melakukan zip
chdir("..");
// zipping terhadap direktori
char zipname[30];
pid_t pid;
    
pid = fork();
int done = 0;

if (pid == -1) {
    perror("fork failed");
    exit(EXIT_FAILURE);
} else if (pid == 0) {
sprintf(zipname, "%s.zip", foldername);
execl("/usr/bin/zip", "zip", "-r", zipname,
foldername, NULL);
exit(EXIT_SUCCESS);
}

int status;
waitpid(pid, &status, 0);

pid = fork();
if (pid == -1) {
perror("fork failed");
exit(EXIT_FAILURE);
} else if (pid == 0) {
    sprintf(zipname, "%s.zip", foldername);
    execl("/usr/bin/rm", "rm", "-R", foldername, NULL);
    exit(EXIT_SUCCESS);
    }
```
###### Penjelasan
waitpid memastikan program berjalan menunggu selesainya eksekusi program child

##### Point d
Program killer perlu dibuat untuk memastikan program tadi tidak lepas kendali. program killer akan menterminasi semua operasi program ketika di run, dan akan mendelete dirinya sendiri.
###### Solusi
Dilakukan pembuatan program killer secara otomatis saat program berjalan dengan isi SIGTERM, dan SIGSTOP yang siap dirunning
###### Code
```sh
FILE *fp;
    fp = fopen("terminator.c", "w");
    fprintf(fp,[command], pid);
```
###### Penjelasan
fprintf (FILE* stream, const char* format,...) berfungsi untuk menginput string kedalam sebuah file, baik berupa tulisan normal ataupun kode format.

##### Point e
Program utama bisa dirun dalam dua mode, yaitu MODE_A
dan MODE_B. untuk mengaktifkan MODE_A, program harus
dijalankan dengan argumen -a. Untuk MODE_B, program harus
dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A,
program utama akan langsung menghentikan semua operasinya ketika
program killer dijalankan. Untuk MODE_B, ketika program killer
dijalankan, program utama akan berhenti tapi membiarkan proses di
setiap folder yang masih berjalan sampai selesai(semua folder terisi
gambar, terzip lalu di delete).

###### Solusi
Program killer tersedia dengan 2 varian tergantung mode yang dipilih
###### Code
```sh
if (strcmp(argv[1], "-a") == 0) {
            FILE *fp;
            fp = fopen("terminator.c", "w");
            fprintf(fp, "#include <unistd.h>\n\nint main() {\n    
            execl(\"/bin/kill\", \"kill\", \"-s\", \"SIGKILL\", \"%d\", NULL);\n    
            return 0;\n}", pid);
            fclose(fp);
        } else if (strcmp(argv[1], "-b") == 0) {
            FILE *fp;
            fp = fopen("terminator.c", "w");
            fprintf(fp, "#include <unistd.h>\n\nint main() {\n    
            pid_t pid;\n    
            pid = fork();\n    
            if (pid == 0) {\n        
                execl(\"/bin/kill\", \"kill\", \"-SIGTERM\", \"%d\", NULL);\n 
            }\n 
            return 0;\n}", pid, pid);
            fclose(fp);
        }
```
###### Penjelasan
SIGTERM (15): meminta program untuk berhenti bekerja dan memberikannya waktu untuk menyimpan semua progress.
SIGKILL (9): memaksa program untuk berhenti bekerja saat itu juga. Progress yang tidak tersimpan akan hilang.




### Problem 3
##### Penjelasan Soal
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut

##### Point A
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
###### Solusi
Untuk menyelesaikan permintaan tersebut maka akan dibuatkan suatu program untuk mengunduh dan mengekstrak "players.ip" dan setelah selesai maka akan dihapus

###### Code
```
nano filter.c
```
`nano filter.c` digunakan untuk membuat suatu file bernama filter.c

###### Melakukan Download
```
void fileManagemen(){
    char file_url[] = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char file_name[] = "file.zip"; //file yang akan diunduh
    child_pid = fork();
    if(child_pid == 0) {
        execlp("wget", "wget", "-O", file_name, file_url, NULL); 
    }
```
1. `char file_url[]` dan `char file_name[]` akan dideklarasikan sebuah nama dengan Array bertipe char
2. `child_pid = fork();` untuk membuat proses child dengan menggunakan fungsi fork(). Nilai PID dari proses child akan disimpan ke dalam variabel child_pid. 
3. `if(child_pid == 0)` untuk mengecek apakah child_pid adalah 0. Jika iya maka akan lanjut eksekusi 
4. `execlp("wget", "wget", "-O", file_name, file_url, NULL);` wget merupakan singkatan dari WEB GET digunakan untuk mengunduh file dari internet. `wget` pertama akan dieksekusi oleh execlp. `wget` kedua merupakan argumen untuk -O, `-O` digunakan untuk menentukan nama file yang akan disimpan dan URL. `NULL` menyatakan bahwa tidak ada argumen lagi

###### Melakukan Unzip
```
    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0) {
        execlp("unzip", "unzip", file_name, NULL);
    }
```
1. `while ((wait(&status)) > 0);` tunggu proses child sebelumnya selesai lalu akan dieksekusi
2. `execlp("unzip", "unzip", file_name, NULL);` digunakan untuk unzip sebuah file_name

###### Melakukan Hapus File 
```
    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0) {
        execlp("rm", "rm", file_name, NULL);
    }
```
1. `execlp("rm", "rm", file_name, NULL)` rm merupakan singkatan dari REMOVE digunakan untuk menghapus file bernama file_name

##### Point B
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
###### Solusi
Untuk menyelesaikan problem tersebut maka kita akan menghapus pemain yang bukan dai Manchester United

###### Code
```
void hapusbukanMU(char path[], char filename[]) { 

    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-type", "f", "!", "-name", filename, "-delete", NULL);
    }
}

```
1. `execlp("find", "find", path, "-type", "f", "!", "-name", filename, "-delete", NULL);` Find digunakan untuk mencari suatu file/direktori yang dimana disini adalah path. `-type f` untuk mencari file, `! -name filename` digunakan untuk mengabaikan file dengan nama tertentu, `-delete` untuk menghapus file yang memenuhi kriteria

##### Point C
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
###### Solusi
Untuk menyelesaikan masalah tersebut maka kita akan membuat kategori menjadi 4 folder yang berbeda

###### Code
```
void posisiGroup(char path[]) {
    buatGroup();
    while ((wait(&status)) > 0);

    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Penyerang*", "-exec", "mv", "-n", "{}", "./players/penyerang/", ";", NULL);
    }

    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Bek*", "-exec", "mv", "-n", "{}", "./players/bek/", ";", NULL);
    }

    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Gelandang*", "-exec", "mv", "-n", "{}", "./players/gelandang/", ";", NULL);
    }

    child_pid = fork();
    if (child_pid == 0) {
        execlp("find", "find", path, "-iname", "*Kiper*", "-exec", "mv", "-n", "{}", "./players/kiper/", ";", NULL);
    }
}
```
1. `buatGroup()` memanggil fungsi buatGroup
2. `execlp("find", "find", path, "-iname", "*Penyerang*", "-exec", "mv", "-n", "{}", "./players/penyerang/", ";", NULL);` mencari file-file dalam suatu direktori dengan kata kunci "Penyerang", kemudian memindahkan file-file tersebut ke direktori "./players/penyerang/".
    - execlp: fungsi yang digunakan untuk menjalankan perintah di dalam proses yang baru dibuat.
    - "find": perintah yang akan dieksekusi menggunakan execlp.
    - "find": argumen pertama untuk execlp, yang merupakan nama perintah yang akan dijalankan.
    - path: argumen kedua untuk execlp, yaitu direktori tempat mencari file dengan kata kunci tertentu.
    - "-iname": argumen ketiga untuk execlp, yang menunjukkan bahwa pencarian file akan dilakukan dengan tidak memperhatikan huruf besar atau kecil.
    - "*Penyerang*": argumen keempat untuk execlp, yaitu kata kunci pencarian file yang dicari dalam direktori.
    - "-exec": argumen kelima untuk execlp, menunjukkan bahwa perintah yang ditentukan setelahnya akan dijalankan pada setiap file yang cocok dengan pencarian sebelumnya.
    - "mv": argumen keenam untuk execlp, menunjukkan perintah yang akan dijalankan pada setiap file yang cocok dengan pencarian sebelumnya.
    - "-n": argumen ketujuh untuk execlp, menunjukkan bahwa file yang cocok tidak akan diganti jika file dengan nama yang sama sudah ada di direktori tujuan.
    - "{}": argumen kedelapan untuk execlp, yaitu file yang cocok dengan pencarian sebelumnya.
    - "./players/penyerang/": argumen kesembilan untuk execlp, yaitu direktori tujuan di mana file akan dipindahkan.
    - ";": argumen kesepuluh untuk execlp, menunjukkan bahwa perintah "find" telah selesai dijalankan.
    - NULL: argumen terakhir untuk execlp, menandakan akhir dari daftar argumen. 


##### Point D
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
###### Solusi
kita akan membuat coding untuk membuat formasi tersebut dengan output berupa  file txt dan akan ditaruh di halaman utama

###### Code
```
void buatTim(int bek, int gelandang, int penyerang){

    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/kiper | sort -t _ -nk4 | tail -n 1 >> ~/Formation_%d_%d_%d.txt", bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/bek | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",bek, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/gelandang | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",gelandang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_pid = fork();
    if(child_pid == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/penyerang | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",penyerang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    printf("Done creating the team formation\n");
}
```
1. `char cmd[100];` Mendeklarasikan sebuah variabel bertipe array of char dengan nama cmd. Ukuran array tersebut adalah 100 karakter. Variabel cmd ini akan digunakan untuk menyimpan perintah yang akan dieksekusi menggunakan execlp() pada baris selanjutnya.
2. `snprintf(cmd, sizeof(cmd), "ls ./players/bek | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt",bek, bek, gelandang, penyerang);`
    - `snprintf` digunakan untuk memformat string dengan menggunakan sintaks printf ke dalam buffer cmd
    - `sizeof(cmd)` digunakan untuk menentukan ukuran maksimal dari buffer cmd agar tidak terjadi buffer overflow.
    - `"ls ./players/bek | sort -t _ -nk4 | tail -n %d >> ~/Formation_%d_%d_%d.txt":` Perintah ini mengambil daftar file pada direktori ./players/bek, 
    - `sort -t _ -nk4` mengurutkan berdasarkan angka di posisi ke-4 dalam nama file
    - `tail -n %d`memilih jumlah pemain bek. Jumlah pemain bek diambil dari variabel bek.
    - `>>` menambahkan output ke file tanpa menghapus isinya terlebih dahulu jika file sudah ada.
    - `~/Formation_%d_%d_%d.txt` File hasilnya akan disimpan pada file Formation_%d_%d_%d.txt, dengan %d diganti oleh variabel bek, gelandang, dan penyerang
3. `execlp("sh", "sh", "-c", cmd, NULL);` dilakukan pemanggilan terhadap shell untuk menjalankan sebuah perintah. 
    `sh` adalah nama shell yang digunakan untuk menjalankan perintah 
    `"-c"` adalah opsi untuk mengeksekusi perintah yang diberikan sebagai argumen 
    `cmd` adalah perintah yang akan dieksekusi 
    `NULL` adalah argumen terakhir yang diberikan ke fungsi execlp.

##### Eksekusi
```
int main() {
    int status;

    fileManagemen();

    hapusbukanMU("./players", "*ManUtd*");

    posisiGroup("./players");

    buatTim(4, 3, 3);

    return 0;
}
```
1. `int main()` merupakan fungsi utama yang akan dieksekusi
2. `int status` mendefinisikan variabel status sebagai integer
3. `fileManagemen()` Memanggil fungsi fileManagemen() untuk mengunduh dan mengekstrak file zip yang dibutuhkan
4. `hapusbukanMU("./players", "*ManUtd*")` Fungsi ini akan menghapus semua file dalam direktori "./players" yang tidak mengandung kata "ManUtd" pada namanya
5. `posisiGroup("./players");` Fungsi ini akan mengelompokkan pemain ke dalam direktori yang sesuai dengan posisi mereka (penyerang, bek, gelandang, kiper)
6. `buatTim(4, 3, 3)`memanggil fungsi createTeam dengan argumen jumlah pemain untuk posisi bek (4), gelandang (3), dan penyerang (3). Fungsi ini akan membuat file teks berisi susunan pemain dalam formasi tertentu.
7. `return 0` mengembalikan nilai 0, menandakan program telah berhasil dijalankan


#### Problem 4
##### Penjelasan Soal
Diminta membuat program untuk menjalankan script bash menyerupai crontab dengan bahasa C. 
##### Point a
Program menerima input argumen berupa berupa Jam (0-23), Menit (0-59),
Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
Program juga mengeluarkan pesan error jika input tidak sesuai format
###### Solusi
sesuai cara kerja crontab yang membuat program bekerja dalam jangka waktu tertentu. Jadi input waktu yang diterima digunakan sebagai timer crontab.
###### Code
```sh
if (argc != 5)
  {
    // exp ./nomorEmpat 0 0 15 /home/Documents/sisop/P2S2/hellow.sh
    puts("Tolong input dalam format [jam/'*'] [menit/'*'] [detik/'*'] [filePath]");
    return 1;
  }
===============
  char *strHour = argv[1];
  char *strMin = argv[2];
  char *strSec = argv[3];
  char *filePath = argv[4];

  int hour, mins, secs;

  bool error = false;
  if (strHour[0] == '*')
    hour = -1;
  else
  {
    // convert string hour menjadi integer
    hour = atoi(strHour);

    if (hour > 23 || hour < 0){
      puts("Gaada jam segitu masbro");
      error = true;
    }
  }

  if (strMin[0] == '*')
    mins = -1;
  else
  {
    mins = atoi(strMin);

    if (mins > 59 || mins < 0){
      puts("Gaada menit segitu masbro");
      error = true;
    }
  }

  if (strSec[0] == '*')
    secs = -1;
  else
  {
    secs = atoi(strSec);

    if (secs > 59 || secs < 0){
      puts("Gaada detik segitu masbro");
      error = true;
    }
  }
 =================
  pid_t pid;
  pid = fork();

  if (pid < 0) exit(0);
  else if (pid > 0) exit(1);
  else {
    
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    umask(0);

    pid_t executesid = setsid();
    if (executesid < 0) exit(0);

    while (1)
    {
      time_t now = time(NULL);
      struct tm *tm_now = localtime(&now);

      if ((hour == -1 || tm_now->tm_hour == hour) && (mins == -1 || tm_now->tm_min == mins) && (secs == -1 || tm_now->tm_sec == secs))
      {
        pid_t pid2 = fork();

        if (!pid2)
        {
          execl("/usr/bin/bash", "bash", filePath, NULL);
          exit(0);
        }
      }
      sleep(1);
    }
  }
```
###### Penjelasan
Pada segmen pertama, pesan error akan keluar jika argumen yang diinputkan kurang dari 5 (run, hour, min, sec, path).
(argc!=5)

lalu pada segmen ke 2, input waktu diubah menjadi integer dengan atoi, dan kemudian masuk kedalam branch untuk filter.

terakhir pada segmen 3, umask() untuk set permission directory default, setsid untuk running program pada sesi baru. 
