#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

void downloadFile(char *filename, char *url){
    // download file "bintang.zip" dari Google Drive
    pid_t child = fork();
    if(child == 0){ //child yang akan dijalankan
        char *argv[] = {"wget", "-O", filename, url, "&", NULL};
        execv("/usr/bin/wget", argv);
        /*
        execv = untuk menggantikan image program saat ini dengan
                image program baru yang disebut dalam argumen pertama tadi.

        & = untuk menjalankan command wget dalam background, sehingga child
            process dapat berlanjut tanpa menunggu operasi download selesai
        */
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
}
void unzipFile(char *directory){
    // unzip file "binatang.zip" yang sudah di download
    pid_t child = fork();
    if(child == 0){ //child yang akan dijalankan
        char *argv[] = {"unzip", directory, NULL};
        execv("/usr/bin/unzip", argv);
        /*
        execv = untuk menggantikan image program saat ini dengan
                image program baru yang disebut dalam argumen pertama tadi.        
        */
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
}
void getFile(){
    pid_t child = fork();
    if(child == 0){ //child yang akan dijalankan
        struct dirent *entry;
        DIR *directory = opendir(".");
        // membuka direktori tempat program berjalan
        // dan mengembalikan sebuah pointer ke struktur dir
        if(directory == NULL){
            printf("Error open dir");
            exit(EXIT_FAILURE);
        }

        int count = 0;
        // hitung berapa file yang ada
        while ((entry = readdir(directory)) != NULL) {
        // readdir = untuk membaca setiap file dalam direktori tersebut
            if (entry->d_type == DT_REG) {
                count++;
                // +1 count saat setiap kali sebuah file ditemukan
            }
        }
 
        // alokasi memori
        char **files = malloc(count * sizeof(char *));
        // malloc = alokasikan memori
        if (files == NULL) {
            printf("Error malloc");
            exit(EXIT_FAILURE);
        }
        // kembalikan pointer ke file pertama
        rewinddir(directory);
 
        //take name of file name
        int i = 0;
        while ((entry = readdir(directory)) != NULL) {
        /*
        setiap kalo sebuah file ditemukan dan memiliki nama file ".jpg"
        maka nama file tersbut akan disimpan ke dalam array "files"
        */
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".jpg")) {
                files[i] = entry->d_name;
                i++;
            }
        }
        // tutup direktorinya
        closedir(directory);

        // menentukan nilai seed untuk generator bilangan acak
        srand(time(NULL));
        /*
        program akan menentukan nilai seed
        */
        int pickAcak = rand() % i;
 
        // random pick hewan yang harus dijaga
        char penjagaan[50];
        strcpy(penjagaan, files[pickAcak]);
        printf("Hewan yang harus dijaga adalah %s\n", penjagaan);
        // mengembalikan blok memori
        free(files);
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    }
}
void createDirectory(){
  //create directory
    system("mkdir HewanDarat HewanAir HewanAmphibi");
    // pindahkan file
    system("mv *amphibi.jpg HewanAmphibi");
    system("mv *darat.jpg HewanDarat");
    system("mv *air.jpg HewanAir");
}
void zipAnimal(){
    //zip the directory
    pid_t child = fork();
    if(child == 0){ //child process
        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/usr/bin/zip", argv);
        /*
        execv() mengeksekusi program zip dan melakukan kompresi
        pada direktori yang sesuai dengan argumen yang diberikan
        */
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
    child = fork();
    if(child == 0){ //child process
        char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/usr/bin/zip", argv);
        /*
        execv() mengeksekusi program zip dan melakukan kompresi
        pada direktori yang sesuai dengan argumen yang diberikan
        */
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
    child = fork();
    if(child == 0){ //child process
        char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/usr/bin/zip", argv);
        /*
        execv() mengeksekusi program zip dan melakukan kompresi
        pada direktori yang sesuai dengan argumen yang diberikan
        */
        return 0;
    }else if(child < 0){
        exit(EXIT_FAILURE);
    } 
}
void deleteDirectory(){
// untuk menghapus "HewanAir", "Hewandarat", "HewanAmphibi", dan file "bintang.zip"SS

    system("rm -rf HewanAir HewanDarat HewanAmphibi binatang.zip");
    /*
    system() digunakan untuk menjalankan perintah shell pada sistem operasi
    rm = untuk menghapus sebuah file/ direktori
    parameter -r = menghapus direktori secara rekursif 
    parameter -f = memeksa penghapusan tanpa konfirmasi
    */
}

int main(){
    pid_t child_id;
    int status;

    downloadFile("binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq");
    while((wait(&status)) > 0);
    /*
    while((wait(&status)) > 0) = digunakan untuk menunggu semua child process
    selesai dieksekusi sebelum parent process melanjutkan eksekusi selanjutnya.

    wait() digunakan untuk memblokir parent process sampai child process selesai
    dieksekusi.

    Dalam loop ini, parent process akan memanggil wait() dan memeriksa apakah ada 
    child process yang masih berjalan, dan jika ada maka parent process akan menunggu. 
    Setelah semua child process selesai dieksekusi, loop akan berhenti dan parent process 
    akan melanjutkan eksekusi selanjutnya. Dalam program yang menggunakan multiple 
    child process, wait() diperlukan agar parent process tidak menyelesaikan eksekusi 
    sebelum semua child process selesai dieksekusi.
    */

    unzipFile("binatang.zip");
    while((wait(&status)) > 0);
    /*
    while((wait(&status)) > 0) = digunakan untuk menunggu semua child process
    selesai dieksekusi sebelum parent process melanjutkan eksekusi selanjutnya.

    wait() digunakan untuk memblokir parent process sampai child process selesai
    dieksekusi.

    Dalam loop ini, parent process akan memanggil wait() dan memeriksa apakah ada 
    child process yang masih berjalan, dan jika ada maka parent process akan menunggu. 
    Setelah semua child process selesai dieksekusi, loop akan berhenti dan parent process 
    akan melanjutkan eksekusi selanjutnya. Dalam program yang menggunakan multiple 
    child process, wait() diperlukan agar parent process tidak menyelesaikan eksekusi 
    sebelum semua child process selesai dieksekusi.
    */

    getFile();
    while((wait(&status)) > 0);
    /*
    while((wait(&status)) > 0) = digunakan untuk menunggu semua child process
    selesai dieksekusi sebelum parent process melanjutkan eksekusi selanjutnya.

    wait() digunakan untuk memblokir parent process sampai child process selesai
    dieksekusi.

    Dalam loop ini, parent process akan memanggil wait() dan memeriksa apakah ada 
    child process yang masih berjalan, dan jika ada maka parent process akan menunggu. 
    Setelah semua child process selesai dieksekusi, loop akan berhenti dan parent process 
    akan melanjutkan eksekusi selanjutnya. Dalam program yang menggunakan multiple 
    child process, wait() diperlukan agar parent process tidak menyelesaikan eksekusi 
    sebelum semua child process selesai dieksekusi.
    */

    createDirectory();
    while((wait(&status)) > 0);
    /*
    while((wait(&status)) > 0) = digunakan untuk menunggu semua child process
    selesai dieksekusi sebelum parent process melanjutkan eksekusi selanjutnya.

    wait() digunakan untuk memblokir parent process sampai child process selesai
    dieksekusi.

    Dalam loop ini, parent process akan memanggil wait() dan memeriksa apakah ada 
    child process yang masih berjalan, dan jika ada maka parent process akan menunggu. 
    Setelah semua child process selesai dieksekusi, loop akan berhenti dan parent process 
    akan melanjutkan eksekusi selanjutnya. Dalam program yang menggunakan multiple 
    child process, wait() diperlukan agar parent process tidak menyelesaikan eksekusi 
    sebelum semua child process selesai dieksekusi.
    */

    zipAnimal();
    while((wait(&status)) > 0);
    /*
    while((wait(&status)) > 0) = digunakan untuk menunggu semua child process
    selesai dieksekusi sebelum parent process melanjutkan eksekusi selanjutnya.

    wait() digunakan untuk memblokir parent process sampai child process selesai
    dieksekusi.

    Dalam loop ini, parent process akan memanggil wait() dan memeriksa apakah ada 
    child process yang masih berjalan, dan jika ada maka parent process akan menunggu. 
    Setelah semua child process selesai dieksekusi, loop akan berhenti dan parent process 
    akan melanjutkan eksekusi selanjutnya. Dalam program yang menggunakan multiple 
    child process, wait() diperlukan agar parent process tidak menyelesaikan eksekusi 
    sebelum semua child process selesai dieksekusi.
    */

    // hapus semua direktorinya
    deleteDirectory();
    return 0;
}




