#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>

#define MAX_IMAGES 15
#define TIME_INTERVAL_IMAGE 5
#define TIME_INTERVAL_FOLDER 30
#define URL "https://picsum.photos/%ld"

// void exeOne();
// void download_image();
// void compile_stop();

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: %s [-a] or %s [-b]\n", argv[0], argv[0]);
        return 1;
    }

    pid_t pid;
    pid = fork();
    

    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        printf("Daemon pid: %d\n", pid);
        if (strcmp(argv[1], "-a") == 0) {
            FILE *fp;
            fp = fopen("terminator.c", "w");
            fprintf(fp, "#include <unistd.h>\n\nint main() {\n    
            execl(\"/bin/kill\", \"kill\", \"-s\", \"SIGTERM\", \"%d\", NULL);\n    
            return 0;\n}", pid);
            fclose(fp);
        } else if (strcmp(argv[1], "-b") == 0) {
            FILE *fp;
            fp = fopen("terminator.c", "w");
            fprintf(fp, "#include <unistd.h>\n\nint main() {\n    
            pid_t pid;\n    
            pid = fork();\n    
            if (pid == 0) {\n        
                execl(\"/bin/kill\", \"kill\", \"-SIGSTOP\", \"%d\", NULL);\n   
                sleep(1);\n 
                execl(\"/bin/kill\", \"kill\", \"-SIGCONT\", \"%d\", NULL);\n   
            }\n 
            return 0;\n}", pid, pid);
            fclose(fp);
        }
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0)
        exit(EXIT_FAILURE);

    umask(0);
    
    int x;
    for (x = sysconf(_SC_OPEN_MAX); x>=0; x--) {
        close (x);
    }

    compile_stop();

    while (1) {
        exeOne();
        // jeda pembuatan folder
        sleep(30);
    }

    exit(EXIT_SUCCESS);
}

void exeOne() {
    char foldername[20];
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);

    // penamaan folder berdasarkan timestamp
    strftime(foldername, sizeof(foldername), "%Y-%m-%d_%H:%M:%S", tm);
    // buat folder dengan nama yang telah ditentukan barusan
    mkdir(foldername, 0777);
    // masuk ke dalam folder dengan change directory
    chdir(foldername);

    for(int i = 1; i <= 15; i++) {
        download_image();
        sleep(5);
    }

    // keluar dari direktori untuk melakukan zip
    chdir("..");
    // zipping terhadap direktori
    char zipname[30];
    pid_t pid;
    
    pid = fork();
    int done = 0;

    if (pid == -1) {
        perror("fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        sprintf(zipname, "%s.zip", foldername);
        execl("/usr/bin/zip", "zip", "-r", zipname, foldername, NULL);
        exit(EXIT_SUCCESS);
    }

    int status;
    waitpid(pid, &status, 0);

    pid = fork();

    if (pid == -1) {
        perror("fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        sprintf(zipname, "%s.zip", foldername);
        execl("/usr/bin/rm", "rm", "-R", foldername, NULL);
        exit(EXIT_SUCCESS);
    }
}

void download_image() {
    char url[50];
    char *argv[5];
    time_t     now;
    struct tm  ts;
    char       buf[80];

    // Get current time
    time(&now);

    ts = *localtime(&now);
    // deklarasi epoch time
    time_t epoch;
    // penamaan berdasarkan timestamp
    if(strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts)) {
        epoch = mktime(&ts);
    }
    char filename[20];
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S.jpg", tm);
    sprintf(url, URL, epoch%1000 + 50);
    
    pid_t pid;
    
    pid = fork();
            
    if (pid == -1) {
        perror("fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", "-q", "-O", filename, url, NULL);
        exit(EXIT_SUCCESS);
    }
}

void compile_stop() {
    pid_t pid;

    pid = fork();

    if (pid == 0) {
        execl("/usr/bin/gcc", "/usr/bin/gcc", "-o", "terminator", "terminator.c", NULL);
    }
    int status;
    waitpid(pid, &status, 0);
}